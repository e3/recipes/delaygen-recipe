# delaygen conda recipe

Home: https://github.com/epics-modules/delaygen

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS delaygen module
